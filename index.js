var argv = require('minimist')(process.argv.slice(2)),
    App = require('./lib/app');

var app = new App();

if (argv.c) {
  app.cleanDB();
}

if (argv.e) {
  app.getErrors();
}

if (argv.h) {
  app.help();
}

app.start();
