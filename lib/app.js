var redis = require('redis'),
    async = require('async'),
    pkg = require('../package.json'),
    clients = {
      system: null,
      msgClient: null,
      msgGen: null,
      errors: null
    },
    baseKey = pkg._redis.keys.base,
    idKey = baseKey+pkg._redis.keys.id,
    generatorKey = baseKey+pkg._redis.keys.generator,
    messagesKey = baseKey+pkg._redis.keys.messages,
    errorKey = baseKey+pkg._redis.keys.errors,
    sendingDelay = pkg._sendingDelay;

function createRedisClient(type, cb) {
  // console.log(type);
  if (!clients[type]) {
    clients[type] = redis.createClient(pkg._redis.options);
    clients[type].on('error', function (err) {
      // console.error('cant create ', type, ' client with error ', err);
    });
    clients[type].on('ready', function () {
        // console.log('client', type, 'created');
        clients[type].status = true;
        if (cb) cb();
    });
  } else {
    // console.log('client', type, 'exist');
    if (cb) cb();
  }
}

function configureDB() {
  clients.system.set(idKey, 0)
  clients.system.del(generatorKey)
  clients.system.del(messagesKey)
  clients.system.del(errorKey)
  console.log('DB now in initial state');
  process.exit();
}

function killRedisClient(type) {
  if (clients[type]) {
    clients[type].quit();
    clients[type] = null;
  };
}

var App = function() {}

App.prototype.start = function() {
  async.waterfall([
    async.apply(createRedisClient, 'system'),
    setId,
    printId,
    check
  ],
  function(err, results){
    if (err) console.error(err);
  });
};

App.prototype.cleanDB = function() {
  createRedisClient('system', configureDB)
};

App.prototype.getErrors = function() {
  createRedisClient('errors', getAllErrors)
};

App.prototype.help = function() {
  console.log("use keys: -c for clean DB, -e to get errors and remove them from DB");
  process.exit();
}

function check() {
  async.waterfall([
    checkGeneratorExist,
    subPub
  ],
  function(err, results){
    if (err) console.error(err);
  });
}

function beAGenerator() {
  async.waterfall([
    async.apply(createRedisClient, 'msgGen'),
    createGenerator,
    sendMessage
  ], function(err, result) {
    if (err) console.error(err)
  });
}

function beAClient() {
  async.waterfall([
    async.apply(createRedisClient, 'msgClient'),
    checkMsgExist,
    readMsg,
    eventHandler,
    msgHandler,
    print
  ], function(err, result) {
    if (err) console.error(err, result)
  });
}

function passError(err) {
  async.waterfall([
    async.apply(createRedisClient, 'error'),
    async.apply(doSomethingWithError, err)
  ], function(err, result) {
    if (err) console.error(err)
  });
}

var ID = null;

function setId(cb) {
  clients.system.incr(idKey, cb)
}

function printId(reply, cb) {
  ID = reply;
  console.log("current app instance id is: ", ID);
  cb()
}

function checkGeneratorExist(cb) {    
  clients.system.ttl(generatorKey, cb)
}

function subPub(ttl, cb) {
  if (ttl === -2) {
    console.log('subpub ttl', ttl);
    console.log('generator is not exist');
    killRedisClient('msgClient');
    beAGenerator();
  } else {
    beAClient();
  }
}

function checkMsgExist(cb) {
  clients.msgClient.llen(messagesKey, cb)
}

function readMsg(len, cb) {
  if (len) {
    clients.msgClient.rpop(messagesKey, function (rpopErr, msg) {
      cb(null, msg)
    });
  } else {
    setTimeout(check, ID*500);
  }
}

function eventHandler(msg, cb){
  function onComplete(){
    var error = Math.random() > 0.85;
    cb(null, error, msg);
  }
  // processing takes time...
  setTimeout(onComplete, Math.floor(Math.random()*1000));
}

function msgHandler(error, msg, cb) {
  if (error) {
    passError(msg);
  } else {
    if (msg != null) cb(null, msg);
  }
  check();
}

function doSomethingWithError(error) {
  clients.error.rpush(errorKey, error);
  console.log('message ', error, ' is error. Put in DB errors list.');
}

function print(msg) {
  console.log('print', msg);
}

function getMessage(){
  this.cnt = this.cnt || 0;
  return this.cnt++;
}

function createGenerator(cb) {
  clients.msgGen.set(generatorKey, ID);
  console.log("i'm now generator");
  cb();
}

function sendMessage() {
  clients.msgGen.expire(generatorKey, sendingDelay*2/1000);
  var message = getMessage();
  clients.msgGen.lpush(messagesKey, message);
  console.log('send message', message);
  delete message;
  setTimeout(sendMessage, sendingDelay);
}

function getAllErrors() {
  clients.errors.lrange(errorKey, 0, -1, function (err, reply) {
    if (!err) print(reply);
    clients.errors.del(errorKey);
    process.exit();
  });
}

module.exports = App;
